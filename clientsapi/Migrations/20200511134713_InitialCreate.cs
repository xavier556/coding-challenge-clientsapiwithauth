﻿using Microsoft.EntityFrameworkCore.Migrations;
#pragma warning disable CS1591
namespace ClientsApi.Migrations
{
	public partial class InitialCreate : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "Clients",
				columns: table => new
				{
					id = table.Column<int>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					firstname = table.Column<string>(maxLength: 100, nullable: false),
					lastname = table.Column<string>(maxLength: 100, nullable: false),
					address = table.Column<string>(maxLength: 1000, nullable: false),
					email = table.Column<string>(nullable: false),
					phone = table.Column<string>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Clients", x => x.id);
				});

			migrationBuilder.CreateTable(
				name: "Skills",
				columns: table => new
				{
					id = table.Column<int>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					name = table.Column<string>(maxLength: 100, nullable: false),
					level = table.Column<string>(maxLength: 20, nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Skills", x => x.id);
				});

			migrationBuilder.CreateTable(
				name: "ClientSkill",
				columns: table => new
				{
					clientId = table.Column<int>(nullable: false),
					skillId = table.Column<int>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ClientSkill", x => new { x.clientId, x.skillId });
					table.ForeignKey(
						name: "FK_ClientSkill_Clients_clientId",
						column: x => x.clientId,
						principalTable: "Clients",
						principalColumn: "id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_ClientSkill_Skills_skillId",
						column: x => x.skillId,
						principalTable: "Skills",
						principalColumn: "id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateIndex(
				name: "IX_ClientSkill_skillId",
				table: "ClientSkill",
				column: "skillId");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "ClientSkill");

			migrationBuilder.DropTable(
				name: "Clients");

			migrationBuilder.DropTable(
				name: "Skills");
		}
	}
}
