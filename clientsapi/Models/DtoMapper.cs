using System.Linq;
using AutoMapper;
using ClientsApi.Models;
using ClientsApi.Models.Dto;

/// <summary>
/// Define mappings from models to dto objects.
/// </summary>
public class DtoMapper : Profile
{
	/// <summary>
	/// Constructor
	/// </summary>
	public DtoMapper()
	{
		CreateMap<Client, ClientDto>()

			// Map the related skills.
			.ForMember(dto => dto.skills, opt => opt.MapFrom(src => src.skills.Select(s => s.skill)))

			// The fullname is composed of the first and last name.
			.ForMember(dto => dto.fullname, opt => opt.MapFrom(src => src.firstname + " " + src.lastname));


		CreateMap<Client, CreateClientDto>();
		CreateMap<Client, UpdateClientDto>();

		CreateMap<CreateClientDto, Client>();

		CreateMap<UpdateClientDto, Client>();

		CreateMap<Skill, SkillDto>();
		CreateMap<SkillDto, Skill>();
	}
}