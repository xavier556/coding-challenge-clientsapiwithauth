using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientsApi.Models.Dto
{

	/// <summary>
	/// Data transfer object for response returning client(s).
	/// </summary>
	public class ClientDto
	{
		///<value>Record unique id</value>
		public int id { get; set; }

		///<value>Client first name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string firstname { get; set; }

		///<value>Client last name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string lastname { get; set; }

		///<value>Client full name</value>
		[Required]
		[StringLength(200, MinimumLength = 2)]
		public string fullname { get; set; }

		///<value>Client address</value>
		[Required]
		[StringLength(1000)]
		public string address { get; set; }

		///<value>Client email</value>
		[Required]
		[EmailAddress]
		public string email { get; set; }

		///<value>Client phone number</value>
		[Required]
		[Phone]
		public string phone { get; set; }

		///<value>List of client skills</value>
		public List<SkillDto> skills { get; set; }
	}

	/// <summary>
	/// Data transfer object to create a client.
	/// </summary>
	public class CreateClientDto
	{
		///<value>Client first name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string firstname { get; set; }

		///<value>Client last name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string lastname { get; set; }

		///<value>Client address</value>
		[Required]
		[StringLength(1000)]
		public string address { get; set; }

		///<value>Client email</value>
		[Required]
		[EmailAddress]
		public string email { get; set; }

		///<value>Client phone number</value>
		[Required]
		[Phone]
		public string phone { get; set; }
	}

	/// <summary>
	/// Data transfer object to update client.
	/// </summary>
	public class UpdateClientDto
	{
		///<value>Record unique id</value>
		public int id { get; set; }

		///<value>Client first name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string firstname { get; set; }

		///<value>Client last name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string lastname { get; set; }

		///<value>Client address</value>
		[Required]
		[StringLength(1000)]
		public string address { get; set; }

		///<value>Client email</value>
		[Required]
		[EmailAddress]
		public string email { get; set; }

		///<value>Client phone number</value>
		[Required]
		[Phone]
		public string phone { get; set; }
	}
}