using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientsApi.Models;
using ClientsApi.Models.Dto;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace ClientsApi.Controllers
{

	/// <summary>
	/// Controller for the skill endpoint.
	/// </summary>
	[Produces("application/json")]
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class SkillsController : ControllerBase
	{
		private readonly SqliteDbContext _context;

		private readonly IMapper _dtoMapper;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="context">Database context.</param>
		/// <param name="dtoMapper">To map models to dtos.</param>
		public SkillsController(SqliteDbContext context, IMapper dtoMapper)
		{
			_context = context;
			_dtoMapper = dtoMapper;
		}

		/// <summary>
		///	Get the list of all skills.
		/// </summary>
		/// <returns>A collection of skills.</returns>
		[HttpGet]
		[SwaggerResponse(200, "Success", typeof(IEnumerable<ClientDto>))]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<IEnumerable<SkillDto>>> GetSkills()
		{
			var skills = await _context.Skills.ToListAsync();
			return skills.Select(s => _dtoMapper.Map<SkillDto>(s)).ToList();
		}

		/// <summary>
		///	Get a skill by id.
		/// </summary>
		/// <returns>A skill.</returns>
		[HttpGet("{id}")]
		[SwaggerResponse(200, "Success", typeof(ClientDto))]
		[SwaggerResponse(404, "The skill was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<SkillDto>> GetSkill(int id)
		{
			var skill = await _context.Skills.FindAsync(id);

			if (skill == null)
			{
				return NotFound();
			}

			return _dtoMapper.Map<SkillDto>(skill);
		}

		/// <summary>
		///	Update a skill.
		/// </summary>
		[HttpPut("{id}")]
		[SwaggerResponse(204, "Skill was updated")]
		[SwaggerResponse(400, "Skill data is invalid")]
		[SwaggerResponse(404, "The skill was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<IActionResult> PutSkill(int id, SkillDto skillDto)
		{
			// map to model
			var skill = _dtoMapper.Map<Skill>(skillDto);

			if (id != skill.id)
			{
				return BadRequest();
			}

			_context.Entry(skill).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!SkillExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		/// <summary>
		///	Create a new skill.
		/// </summary>
		/// <returns>Return the newly created skill.</returns>
		[HttpPost]
		[SwaggerResponse(201, "The skill was created", typeof(SkillDto))]
		[SwaggerResponse(400, "The skill data is invalid")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<SkillDto>> PostSkill(SkillDto skillDto)
		{
			// map dto to model
			var skill = _dtoMapper.Map<Skill>(skillDto);

			// add to database
			_context.Skills.Add(skill);
			await _context.SaveChangesAsync();

			// return dto
			return CreatedAtAction("GetSkill", new { id = skill.id }, _dtoMapper.Map<SkillDto>(skill));
		}

		/// <summary>
		/// Delete a skill.
		/// </summary>
		/// <returns>Return the deleted skill.</returns>
		[HttpDelete("{id}")]
		[SwaggerResponse(200, "The skill was deleted", typeof(SkillDto))]
		[SwaggerResponse(404, "The skill was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<SkillDto>> DeleteSkill(int id)
		{
			var skill = await _context.Skills.FindAsync(id);
			if (skill == null)
			{
				return NotFound();
			}

			_context.Skills.Remove(skill);
			await _context.SaveChangesAsync();

			return _dtoMapper.Map<SkillDto>(skill);
		}

		private bool SkillExists(int id)
		{
			return _context.Skills.Any(e => e.id == id);
		}
	}
}
