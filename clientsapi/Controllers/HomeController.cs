﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

#pragma warning disable CS1591
namespace mvc_client.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		[Route("index.html")]   // TODO setup default route
		[Route("home")]
		[ApiExplorerSettings(IgnoreApi = true)]
		public IActionResult Index()
		{
			return View();
		}

		[Route("home/logout")]
		[ApiExplorerSettings(IgnoreApi = true)]
		public IActionResult Logout()
		{
			return SignOut("Cookies", "oidc");
		}

		[Route("home/login")]
		[Authorize]
		[ApiExplorerSettings(IgnoreApi = true)]
		public IActionResult Login()
		{
			return View("Views/Home/Index.cshtml");
		}
	}
}
