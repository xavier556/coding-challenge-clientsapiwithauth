using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using ClientsApi.Models;
using ClientsApi.Models.Dto;
using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace ClientsApi.Controllers
{

	/// <summary>
	/// Controller for the client endpoint.
	/// </summary>
	[Produces("application/json")]
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ClientsController : ControllerBase
	{
		private readonly SqliteDbContext _context;

		private readonly IMapper _dtoMapper;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="context">Database context.</param>
		/// <param name="dtoMapper">To map models to dtos.</param>
		public ClientsController(SqliteDbContext context, IMapper dtoMapper)
		{
			_context = context;
			_dtoMapper = dtoMapper;
		}

		/// <summary>
		///	Get the list of all clients.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///     GET /api/Clients
		/// </remarks>
		/// <returns>A collection of clients.</returns>
		[HttpGet]
		[SwaggerResponse(200, "Success", typeof(IEnumerable<ClientDto>))]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<IEnumerable<ClientDto>>> GetClients()
		{
			// get all clients and include skills
			var clients = await _context.Clients
				.Include(c => c.skills).ThenInclude(cs => cs.skill)
				.ToListAsync();

			// map to dto objects
			return clients.Select(c => _dtoMapper.Map<ClientDto>(c)).ToList();
		}

		/// <summary>
		///	Get a client by id.
		/// </summary>
		/// <returns>A client.</returns>
		[HttpGet("{id}")]
		[SwaggerResponse(200, "Success", typeof(ClientDto))]
		[SwaggerResponse(404, "Client was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<ClientDto>> GetClient(int id)
		{
			// Get the client and add its skills.
			var client = await _context.Clients
				.Include(c => c.skills).ThenInclude(cs => cs.skill)
				.FirstOrDefaultAsync(c => c.id == id);

			if (client == null)
			{
				return NotFound();
			}

			// map the result to a dto.
			return _dtoMapper.Map<ClientDto>(client);
		}

		/// <summary>
		///	Update a client.
		/// </summary>
		[HttpPut("{id}")]
		[SwaggerResponse(204, "Client was updated")]
		[SwaggerResponse(400, "Client data is invalid")]
		[SwaggerResponse(404, "Client was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<IActionResult> PutClient(int id, UpdateClientDto updateClientDto)
		{
			var client = _dtoMapper.Map<Client>(updateClientDto);

			if (id != client.id)
			{
				return BadRequest();
			}

			_context.Entry(client).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!ClientExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		/// <summary>
		///	Create a new client.
		/// </summary>
		/// <returns>Return the newly created client.</returns>
		[HttpPost]
		[SwaggerResponse(201, "The client was created", typeof(ClientDto))]
		[SwaggerResponse(400, "The client data is invalid")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<ClientDto>> PostClient(CreateClientDto createClientDto)
		{
			// map dto to model
			var client = _dtoMapper.Map<Client>(createClientDto);

			// add to database
			try
			{
				_context.Clients.Add(client);
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				return StatusCode(StatusCodes.Status500InternalServerError);
			}


			// return dto
			var clientDto = _dtoMapper.Map<ClientDto>(client);

			var action = CreatedAtAction("GetClient", new { id = client.id }, clientDto);
			Console.WriteLine(action.Value);
			return action;
		}

		/// <summary>
		/// Delete a client.
		/// </summary>
		/// <returns>Return the deleted client.</returns>
		[HttpDelete("{id}")]
		[SwaggerResponse(200, "The client was deleted", typeof(ClientDto))]
		[SwaggerResponse(404, "The client was not found")]
		[SwaggerResponse(500, "Internal error")]
		public async Task<ActionResult<ClientDto>> DeleteClient(int id)
		{
			var client = await _context.Clients.FindAsync(id);
			if (client == null)
			{
				return NotFound();
			}

			_context.Clients.Remove(client);
			await _context.SaveChangesAsync();

			// Return a dto.
			return _dtoMapper.Map<ClientDto>(client);
		}

		private bool ClientExists(int id)
		{
			return _context.Clients.Any(e => e.id == id);
		}
	}
}