using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientsApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClientsApi.Controllers
{
	/// <summary>
	/// Controller for the client-skill endpoint.
	/// </summary>
	[Produces("application/json")]
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ClientSkillsController : ControllerBase
	{
		private readonly SqliteDbContext _context;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="context">Database context.</param>
		public ClientSkillsController(SqliteDbContext context)
		{
			_context = context;
		}

		/// <summary>
		///	Get the list of all skills.
		/// </summary>
		/// <returns>A collection of client-skill relations.</returns>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<ClientSkill>>> GetClientSkill()
		{
			return await _context.ClientSkill.ToListAsync();
		}

		/// <summary>
		///	Get a client-skill relation by id.
		/// </summary>
		/// <returns>A ClientSkill relation.</returns>
		[HttpGet("{id}")]
		public async Task<ActionResult<ClientSkill>> GetClientSkill(int id)
		{
			var clientSkill = await _context.ClientSkill.FindAsync(id);

			if (clientSkill == null)
			{
				return NotFound();
			}

			return clientSkill;
		}

		/// <summary>
		///	Update a client-skill relation.
		/// </summary>
		[HttpPut("{id}")]
		public async Task<IActionResult> PutClientSkill(int id, ClientSkill clientSkill)
		{
			if (id != clientSkill.clientId)
			{
				return BadRequest();
			}

			_context.Entry(clientSkill).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!ClientSkillExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		/// <summary>
		///	Create a new client-skill relation.
		/// </summary>
		/// <returns>Return the newly created relation.</returns>
		[HttpPost]
		public async Task<ActionResult<ClientSkill>> PostClientSkill(ClientSkill clientSkill)
		{
			_context.ClientSkill.Add(clientSkill);
			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (ClientSkillExists(clientSkill.clientId))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}

			return CreatedAtAction("GetClientSkill", new { id = clientSkill.clientId }, clientSkill);
		}

		/// <summary>
		/// Delete a client-skill relation.
		/// </summary>
		/// <returns>Return the deleted relation.</returns>
		[HttpDelete("{id}")]
		public async Task<ActionResult<ClientSkill>> DeleteClientSkill(int id)
		{
			var clientSkill = await _context.ClientSkill.FindAsync(id);
			if (clientSkill == null)
			{
				return NotFound();
			}

			_context.ClientSkill.Remove(clientSkill);
			await _context.SaveChangesAsync();

			return clientSkill;
		}

		private bool ClientSkillExists(int id)
		{
			return _context.ClientSkill.Any(e => e.clientId == id);
		}
	}
}
