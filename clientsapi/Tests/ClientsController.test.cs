using Xunit;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using ClientsApi.Controllers;
using ClientsApi.Models;
using ClientsApi.Models.Dto;
using System.Net.Http;
using System.Text;
using System.Net;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;

#pragma warning disable CS1591
namespace ClientsApi.Tests
{
	public class ClientsControllerTest
	{
		public ClientsControllerTest()
		{
			// Add some data to the database.
			Seed();

			// Create the mapper used by controllers.
			var myProfile = new DtoMapper();
			var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
			dtoMapper = new Mapper(configuration);

			// Create http client to call endpoints.
			var server = new TestServer(new WebHostBuilder()
				.UseStartup<ClientsApi.Startup>()
			);
			_client = server.CreateClient();
		}

		protected IMapper dtoMapper;

		private HttpClient _client;

		private void Seed()
		{
			using (var context = new SqliteDbContext())
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var one = TestHelper.createTestClient(1);
				var two = TestHelper.createTestClient(2);

				context.AddRange(one, two);
				context.SaveChanges();

				var skill1 = new Skill();
				skill1.name = "skill1";
				skill1.level = "level1";

				var skill2 = new Skill();
				skill2.name = "skill2";
				skill2.level = "level2";

				context.AddRange(skill1, skill2);
				context.SaveChanges();

				var clientSkill11 = new ClientSkill();
				clientSkill11.clientId = 1;
				clientSkill11.skillId = 1;
				var clientSkill12 = new ClientSkill();
				clientSkill12.clientId = 1;
				clientSkill12.skillId = 2;

				context.AddRange(clientSkill11, clientSkill12);
				context.SaveChanges();
			}
		}

		[Fact]
		public void GetClients_Should_ReturnAllClients()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var okResult = controller.GetClients().Result;

				// Assert
				var result = Assert.IsType<ActionResult<IEnumerable<ClientDto>>>(okResult);
				Assert.Equal(2, okResult.Value.Count());
			}
		}

		[Fact]
		public void GetClient_Should_ReturnTheClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var okResult = controller.GetClient(1).Result;

				// Assert
				var result = Assert.IsType<ActionResult<ClientDto>>(okResult);
				var client = Assert.IsType<ClientDto>(result.Value);
				Assert.Equal("fn1", client.firstname);
			}
		}

		[Fact]
		public void GetClient_Should_Not_ReturnValueIfNoClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var notFoundResult = controller.GetClient(99999).Result;

				// Assert
				Assert.Null(notFoundResult.Value);
			}
		}

		[Fact]
		public void PutClient_Should_ModifyClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var client = TestHelper.createTestClient(1);
				client.id = 1;
				client.lastname = "modln1";
				var okResult = controller.PutClient(1, dtoMapper.Map<UpdateClientDto>(client)).Result;
				var clientAfter = controller.GetClient(1).Result.Value;

				// Assert
				Assert.Equal(client.lastname, clientAfter.lastname);
			}
		}

		[Fact]
		public void PostClient_Should_CreateAndReturnClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);
				var createClientDto = dtoMapper.Map<CreateClientDto>(TestHelper.createTestClient(3));  // data to post

				// Act
				var okResult = controller.PostClient(createClientDto).Result;

				// Assert
				Assert.IsType<ActionResult<ClientDto>>(okResult);
				var clientDto = Assert.IsType<ClientDto>((okResult.Result as CreatedAtActionResult).Value);
				Assert.Equal(createClientDto.firstname, clientDto.firstname);
			}
		}

		[Fact]
		public void PostClient_Should_Not_ReturnValueIfBadParameters()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);
				var createClientDto = dtoMapper.Map<CreateClientDto>(TestHelper.createTestClient(4));  // data to post
				createClientDto.firstname = null;

				// Act
				var badResult = controller.PostClient(createClientDto).Result;

				// Assert
				Assert.Null(badResult.Value);
			}
		}

		[Fact]
		public void DeleteClient_Should_DeleteAndReturnClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var clientsBefore = controller.GetClients().Result.Value;
				var deletedClient = controller.DeleteClient(1).Result.Value;
				var clientsAfter = controller.GetClients().Result.Value;

				// Assert
				Assert.IsType<ClientDto>(deletedClient);
				Assert.Equal("fn1", deletedClient.firstname);
				Assert.Equal(clientsBefore.Count() - 1, clientsAfter.Count());
			}
		}

		[Fact]
		public void DeleteClient_Should_Not_DeleteIfNoClient()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var controller = new ClientsController(context, dtoMapper);

				// Act
				var clientsBefore = controller.GetClients().Result.Value;
				var deletedClient = controller.DeleteClient(9).Result.Value;
				var clientsAfter = controller.GetClients().Result.Value;

				// Assert
				Assert.Null(deletedClient);
				Assert.Equal(clientsBefore.Count(), clientsAfter.Count());
			}
		}

		[Fact]
		public async void PostClient_OkRequest()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var createClientDto = dtoMapper.Map<CreateClientDto>(TestHelper.createTestClient(5));  // data to post
				var request = new HttpRequestMessage(new HttpMethod("POST"), "/api/Clients");
				var s = Newtonsoft.Json.JsonConvert.SerializeObject(createClientDto);
				request.Content = new StringContent(s, Encoding.UTF8, "application/json");
				var controller = new ClientsController(context, dtoMapper);

				// Act: get all clients, then send bad post request, then get all clients
				var clientsBefore = controller.GetClients().Result.Value;
				var response = await _client.SendAsync(request);
				var clientsAfter = controller.GetClients().Result.Value;

				// Assert
				// - response should have ok created code
				// - response should have application/json content type
				// - number of clients should be increased by one
				Assert.Equal(HttpStatusCode.Created, response.StatusCode);
				Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
				Assert.Equal(clientsBefore.Count() + 1, clientsAfter.Count());
			}
		}

		[Fact]
		public async void PostClient_BadRequest()
		{
			using (var context = new SqliteDbContext())
			{
				// Arrange
				var createClientDto = dtoMapper.Map<CreateClientDto>(TestHelper.createTestClient(3));  // data to post
				createClientDto.firstname = null;   // set null firstname on purpose (it is required)
				var request = new HttpRequestMessage(new HttpMethod("POST"), "/api/Clients");
				var s = Newtonsoft.Json.JsonConvert.SerializeObject(createClientDto);
				request.Content = new StringContent(s, Encoding.UTF8, "application/json");
				var controller = new ClientsController(context, dtoMapper);

				// Act: get all clients, then send bad post request, then get all clients
				var clientsBefore = controller.GetClients().Result.Value;
				var response = await _client.SendAsync(request);
				var clientsAfter = controller.GetClients().Result.Value;

				// Assert
				// - response should have bad request code
				// - number of clients should be the same
				Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
				Assert.Equal(clientsBefore.Count(), clientsAfter.Count());
			}
		}
	}
}